package com.android.example.debtsMonitor.activities

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.android.example.debtsMonitor.R

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val toolbar = findViewById(R.id.toolbar) as Toolbar
    private val fab = findViewById(R.id.fab) as FloatingActionButton
    private val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
    private val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
    private val navigationView = findViewById(R.id.nav_view) as NavigationView
    private var titleList: List<String> = ArrayList()
    private var activeFragment: Fragment = Fragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        drawer.setDrawerListener(toggle)
        toggle.syncState()
        toolbar.setOnClickListener{
            onBackPressed()
        }
        navigationView.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else if (supportFragmentManager.backStackEntryCount > 0 ){
            fragmentHandling()
        } else{
            AlertDialog.Builder(this).setTitle("Exit")
                    .setMessage("Are you sure that you want to exit")
                    .setNegativeButton("No",null)
                    .setPositiveButton("Yes", DialogInterface.OnClickListener {
                        dialogInterface, i ->
                        var intent = Intent(Intent.ACTION_MAIN)
                        intent.addCategory(Intent.CATEGORY_HOME)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intent)
                    }).create().show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        when (id) {
            R.id.nav_camera -> {

                Toast.makeText(this, "You clicked on Camera", Toast.LENGTH_SHORT).show()
            }
        }
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    fun loadFragment(fragment: Fragment,title: String, isAddToBackStack: Boolean) {

        val fragmentManager: FragmentManager = supportFragmentManager
        val transaction: FragmentTransaction = fragmentManager.beginTransaction();

        if(fragment.isVisible)
            transaction.remove(fragment)
        transaction.hide(activeFragment)

        transaction.replace(R.id.layoutMain,fragment,title);

        if (isAddToBackStack){
            System.out.print("Added to back stack")
            transaction.addToBackStack(fragment.javaClass.simpleName)

        }

        enableButton(isAddToBackStack)
        toolbar.title = title
        activeFragment = fragment
        transaction.commit()
        titleList.plus(title)
    }

    private fun enableButton(enable: Boolean){
        if (enable){
            toggle.isDrawerIndicatorEnabled = false
            actionBar.setDisplayHomeAsUpEnabled(true)

        } else{
            actionBar.setDisplayHomeAsUpEnabled(false)
            toggle.isDrawerIndicatorEnabled = true

        }
    }

    private fun fragmentHandling(){
        supportFragmentManager.popBackStackImmediate()
        if (!titleList.isEmpty())
            titleList.minus(toolbar.title)

        if (!titleList.isEmpty())
            toolbar.title = titleList[titleList.size - 1]

        if (supportFragmentManager.backStackEntryCount == 0)
            enableButton(false)
    }
}